package przyklad.Model;

public class StudentModel {
	private String studentImie;
	private String studentNazwisko;
	private double studentSrednia;

	public void setStudentImie(String studentImie) {
		this.studentImie = studentImie;
	}

	public String getStudentImie() {
		return this.studentImie;
	}

	public void setStudentNazwisko(String studentNazwisko) {
		this.studentNazwisko = studentNazwisko;
	}

	public String getStudentNazwisko() {
		return this.studentNazwisko;
	}

	public void setStudentSrednia(double studentSrednia) {
		this.studentSrednia = studentSrednia;
	}

	public double getStudentSrednia() {
		return this.studentSrednia;
	}

	public StudentModel() {

	}

	public StudentModel(String studentImie, String studentNazwisko,
			double studentSrednia) {
		this.studentImie = studentImie;
		this.studentNazwisko = studentNazwisko;
		this.studentSrednia = studentSrednia;
	}
}