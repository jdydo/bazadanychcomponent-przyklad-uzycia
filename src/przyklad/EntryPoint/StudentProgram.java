package przyklad.EntryPoint;

import java.util.List;

import przyklad.Model.StudentModel;
import przyklad.Predicate.NajlepsiStudenciPredicate;
import przyklad.Predicate.WybierzWszystkichPredicate;
import glowne.BazaDanychComponent.Contract.IBaza;
import glowne.BazaDanychComponent.Implementation.BazaDanych;

public class StudentProgram {

	//Stosujcie obiekty o maksymalnym zagniezdzeniu: (Object1 zawiera (Object2 zawiera primitives))
	//W przeciwnym wypadku przy usuwaniu obiektu Object1 zostan� w bazie �mieci
	
	public static void main(String[] args) {
		IBaza bazaDanych = new BazaDanych();

		// dodawanie
		boolean wynik1 = bazaDanych.dodajObiekt("BazaStudentow.yap", false,
				new StudentModel("Jan", "Kowalski", 4.5));
		wynik1 = bazaDanych.dodajObiekt("BazaStudentow.yap", false,
				new StudentModel("Tadeusz", "Nowak", 3.0));
		wynik1 = bazaDanych.dodajObiekt("BazaStudentow.yap", false,
				new StudentModel("Maria", "Kwiatowska", 5.0));

		if (wynik1) {
			System.out.println("1. OK!");
		} else {
			System.out.println("B��d!");
		}

		// edycja
		boolean wynik2 = bazaDanych.edytujObiekt("BazaStudentow.yap", false,
				new StudentModel("Maria", null, 0), new StudentModel("Maria",
						"Kopytko", 5.0));
		if (wynik2) {
			System.out.println("2. OK!");
		} else {
			System.out.println("B��d!");
		}

		// znajdowanie
		List<StudentModel> lista1 = bazaDanych.znajdzObiekt(
				"BazaStudentow.yap", false, new StudentModel());
		if (lista1 != null && lista1.size() == 3) {
			System.out.println("3. OK!");
		} else {
			System.out.println("B��d!");
		}

		// znajdowanie Predicate
		List<StudentModel> lista2 = bazaDanych.znajdzObiektPredicate(
				"BazaStudentow.yap", false, new NajlepsiStudenciPredicate(4.0));
		if (lista2 != null && lista2.size() == 2) {
			System.out.println("4. OK!");
		} else {
			System.out.println("B��d!");
		}

		// usuwanie pojedynczego rekordu
		boolean wynik3 = bazaDanych.usunObiekt("BazaStudentow.yap", false,
				new StudentModel("Maria", null, 0));
		if (wynik3) {
			System.out.println("5. OK!");
		} else {
			System.out.println("B��d!");
		}

		// usuwanie rekordow spelniajacych warunki (szablon - potrzebny tylko
		// jego typ dla celow wewnetrznych, nie ma wplywu na wyszukiwanie)
		boolean wynik4 = bazaDanych.usunObiektPredicate("BazaStudentow.yap",
				false, new WybierzWszystkichPredicate(), new StudentModel());
		if (wynik4) {
			System.out.println("6. OK!");
		} else {
			System.out.println("B��d!");
		}
	}
}
