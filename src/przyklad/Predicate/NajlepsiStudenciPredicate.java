package przyklad.Predicate;

import przyklad.Model.StudentModel;

import com.db4o.query.Predicate;

public class NajlepsiStudenciPredicate extends Predicate<StudentModel> {

	double dolnyProg;

	public NajlepsiStudenciPredicate(double dolnyProg) {
		this.dolnyProg = dolnyProg;
	}

	@Override
	public boolean match(StudentModel student) {
		return this.dolnyProg <= student.getStudentSrednia();
	}
}
